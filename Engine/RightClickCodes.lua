--[[
 * Copyright (c) 2012-2014 Those Awesome Guys (TAG)
 *
 * You may NOT use this software for any commercial 
 * purpose, nor can you alter and redistribute it as your own.
 * Any credit must be attributed to Those Awesome Guys (TAG)
 * 
 * You are allowed and even encouraged to mod(ify) the game 
 * but all alterations must be specificed as a mod of the game
 * Concerned Joe, and not claim that Concerned Joe itself is a game
 * of your creation.
 *
 *  This notice may not be removed or altered from any source distribution.
 --]]

local RightClickCodes = {};
local UIObject = require("Engine/UIObject");
local currentLevel;
local quads;

function RightClickCodes:init(level)
	currentLevel = level
	if(quads == nil)then 
		local imagemanip = require("Engine/ImageManipulator");
		local o = imagemanip:StripImages("EditorUISheet",nil,nil,nil,{});
		quads = o.quadArray;
	end
  RightClickTools = require("Forge/RightClickTools");
  RightClickTools:init(level);
end

--Useful local window functions
local MakeWindow = function(w,h,name,quads)
	--Makes a window in the centre of the screen
	local window = UIObject:newObject(960 - w/2,350 - h/2,"EditorUISheet","frame",quads["FineJeez_N"])
	window:MakeCustom(w,h,quads,name)
	window:SetModal(true)
	window.onClose = function() _Screen.focus = "game" end
	return window
end

local MakeInput = function(width,Y,window,defaultValue,offX)
	--makes an input field and centers it in the window
	local w = window.width
	local X = w/2 -width/2
	if(offX == nil)then offX = 0 end
	local t = UIObject:newObject(X + offX,Y,"EditorUISheet","textinput")
	t:SetParent(window)
	t.Owidth = width
	t.width = t.Owidth
	t:Rescale()
	t:SetText(defaultValue)
	
	return t;
end

local MakeOkButton = function(Y,window,quads,offX,name)
	local w = window.width
	if(offX == nil)then offX = 0 end
	local ok = UIObject:newObject(w/2,Y,"EditorUISheet","button")
	ok:SetParent(window)
	if(name == nil)then name = "OkLong" end
	ok:SetButtonQuad(name,quads)
	local a,b,wid,h = ok.quad:getViewport()
	ok:SetMyX(w/2 - wid/2 + offX)

	ok.Update = function(self)
		if(_KeyTap["return"] or _cKeyTap["return"])then 
			self:OnClick()
		end
	end

	return ok;
end

local MakeText = function(Y,Text,color,font,window,offX)
	---Makes text that is centered in the window
	local w = window.width 
	local textWidth = _Fonts[font]:getWidth(Text)
	if(offX == nil)then offX = 0 end
	local text = UIObject:newObject(w/2 - textWidth/2 + offX,Y,nil,"text")
	text:SetParent(window)
	text:SetText(Text,color)
	text:SetFont(_Fonts[font])
	text.clickThrough = true
	return text
end

local MakeColorPicker = function(onColorChanged,OnClose,OnOkay,defaultColor)
	local window = MakeWindow(400,500,"Color Picker",quads);
	local Okay = MakeOkButton(400,window,quads)
	local saved = false;
	Okay.OnClick = function(self)
		saved = true;
		window:Destroy()
		OnOkay()
	end

	MakeText(100,"Red",{130,140,140,255},"AlteHaas15",window)
	MakeText(200,"Green",{130,140,140,255},"AlteHaas15",window)
	MakeText(300,"Blue",{130,140,140,255},"AlteHaas15",window)

	local rSlider = UIObject:newObject(130,130,"","slider");
	rSlider:SetWidth(120)
	rSlider:SetMinMax(0, 255)
	rSlider:SetParent(window)
	rSlider:SetValue(defaultColor[1])

	local gSlider = UIObject:newObject(130,230,"","slider");
	gSlider:SetWidth(120)
	gSlider:SetMinMax(0, 255)
	gSlider:SetParent(window)
	gSlider:SetValue(defaultColor[2])

	local bSlider = UIObject:newObject(130,330,"","slider");
	bSlider:SetWidth(120)
	bSlider:SetMinMax(0, 255)
	bSlider:SetParent(window)
	bSlider:SetValue(defaultColor[3])


	rSlider.OnValueChanged = function(object)
		local r,g,b = rSlider:GetValue(),gSlider:GetValue(),bSlider:GetValue()
		onColorChanged(r,g,b)
	end
	bSlider.OnValueChanged = rSlider.OnValueChanged;
	gSlider.OnValueChanged = rSlider.OnValueChanged;

	--has a special close function to destroy the mark
	window.onClose = function(self)
		_Screen.focus = "game"
		if(OnClose ~= nil and not saved)then OnClose() end
	end
end

local createMenu = function(tile)
	local f = (1920/_Screen.width)
	local menu = UIObject:newObject(_Mouse.x * f,_Mouse.y * f,"","panel")
	tile.rightMenu = menu
	tile.innerButtons = {}
	local n = 0;
	for i=1,#tile.clickData do 
		local d = tile.clickData[i]
		local title = d[1];
		local frame = UIObject:newObject(0,n * 30,"EditorUISheet","frame",quads["ContextMenuTitle"])
		frame:SetParent(menu); 
		frame.displayText = title
		frame.displayTextColor = {100,115,123,255}
		n = n + 1
		local selectedName = tile.reverseFunctions[title](tile);
		tile.innerButtons[i] = {}
		for j=2,#d do 
			local button = UIObject:newObject(0,n * 30,"EditorUISheet","button")
			button:SetButtonQuad("ContextMenu",quads)
			button.activeQuad = quads["ContextMenu_O"]
			button.sectorButtons = tile.innerButtons[i];
			button.titleName = title
			button.OnClick = function(self)
				for i=1,#self.sectorButtons do 
					self.sectorButtons[i].Active = false
				end
				self.Active = true
				---run function
				local func = tile.clickFunctions[self.titleName];
				if(tile.OnRightclickRun ~= nil)then 
					tile.OnRightclickRun(func,self.titleName,self.displayText)
				end
				func(tile,self.displayText)
				tile.rightClicked = false
			end
			if(d[j] == selectedName)then button.Active = true end
			button:SetParent(menu);
			button.displayText = d[j]
			button.displayTextColor = {78,102,74,255}
			tile.innerButtons[i][#tile.innerButtons[i]+1] = button
			n = n + 1
		end
	end
end

local Update = function(self)----Update function
	local menu = self.rightMenu
	if(self.rightClicked)then ----------------If right clicking
		if(menu == nil)then 
			createMenu(self)
		end
		if(_mouseUp["l"] or _mouseUp["r"] or _mouseTap["m"])then 
			local touched = false
			for k, v in ipairs(menu.children) do
				if(v.hover)then touched = true; break end
			end
			if(not touched)then self.rightClicked = false end
		end

		for i=1,#self.innerButtons do 
			for j=1,#self.innerButtons[i]do
				local b = self.innerButtons[i][j];
				if(b.hover or b.Active)then 
					b.displayTextColor = {255,255,255,255}
				else 
					b.displayTextColor = {78,102,74,255}
				end
			end
		end
	end -----------------------------End if right clicking

	if(self.rightClicked ~= true)then 
		if(menu ~= nil)then 
			menu:Destroy()
			self.rightMenu = nil
		end
	end
end -------------End update function

RightClickCodes.Door = function(tile)
	---This code initializes the right click for this tile
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}
	tile.clickData[1] = {"State","Open","Close"}
	tile.clickData[2] = {"Restart On Death","Always","Never"}
	tile.clickFunctions["State"] = function(tile,choice)
		local state = "Off";
		if(choice == "Open")then state = "On" end
		tile.dataObject.State.initialState = state
		tile.initialState = state
		tile:Reset()
	end
	tile.reverseFunctions["State"] = function(tile)
		local choice = "Close"
		if(tile.initialState == "On")then choice = "Open" end
		return choice
	end

	tile.clickFunctions["Restart On Death"] = function(tile,choice)
		if(choice == "Always")then 
			tile.neverRestart = nil 
		end 
		if(choice == "Never")then 
			tile.neverRestart = true 
		end
		tile.dataObject.State.neverRestart = tile.neverRestart
	end
	tile.reverseFunctions["Restart On Death"] = function(tile)
		local choice = "Always"
		if(tile.neverRestart)then choice = "Never" end
		return choice
	end

	tile:AddUpdate(Update)
end

RightClickCodes.Box = function(tile)
	---This code initializes the right click for this tile
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}

	tile.clickData[1] = {"Restart On Death","Always","Never"}
	tile.clickFunctions["Restart On Death"] = function(tile,choice)
		if(choice == "Always")then 
			tile.neverRestart = nil 
		end 
		if(choice == "Never")then 
			tile.neverRestart = true 
		end
		tile.dataObject.State.neverRestart = tile.neverRestart
	end
	tile.reverseFunctions["Restart On Death"] = function(tile)
		local choice = "Always"
		if(tile.neverRestart)then choice = "Never" end
		return choice
	end

	tile:AddUpdate(Update)
end

RightClickCodes.ButtonNormal = function(tile)
	---This code initializes the right click for this tile
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}

	tile.clickData[1] = {"Restart On Death","Always","Never"}
	tile.clickFunctions["Restart On Death"] = function(tile,choice)
		if(choice == "Always")then 
			tile.neverRestart = nil 
		end 
		if(choice == "Never")then 
			tile.neverRestart = true 
		end
		tile.dataObject.State.neverRestart = tile.neverRestart
	end
	tile.reverseFunctions["Restart On Death"] = function(tile)
		local choice = "Always"
		if(tile.neverRestart)then choice = "Never" end
		return choice
	end

	tile:AddUpdate(Update)
end

RightClickCodes.ButtonTime = function(tile)
	---This code initializes the right click for this tile
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}

	tile.clickData[1] = {"Restart On Death","Always","Never"}
	tile.clickFunctions["Restart On Death"] = function(tile,choice)
		if(choice == "Always")then 
			tile.neverRestart = nil 
		end 
		if(choice == "Never")then 
			tile.neverRestart = true 
		end
		tile.dataObject.State.neverRestart = tile.neverRestart
	end
	tile.reverseFunctions["Restart On Death"] = function(tile)
		local choice = "Always"
		if(tile.neverRestart)then choice = "Never" end
		return choice
	end

	tile:AddUpdate(Update)
end

RightClickCodes.ButtonJuice = function(tile)
	---This code initializes the right click for this tile
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}
	tile.clickData[1] = {"Amount Of Juice","Small","Medium","High"}
	tile.clickFunctions["Amount Of Juice"] = function(tile,choice)
		local state = "Small";
		if(choice == "Medium")then state = "Med" end
		if(choice == "High")then state = "High" end

		tile.dataObject.State.speedFactor = state
		tile.speedFactor = state
	end
	tile.reverseFunctions["Amount Of Juice"] = function(tile)
		local choice = "Small"
		if(tile.speedFactor == "Med")then choice = "Medium" end
		if(tile.speedFactor == "High")then choice = "High" end
		return choice
	end

	tile:AddUpdate(Update)
end

RightClickCodes.PivotDoor = function(tile)
	---This code initializes the right click for this tile
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}
	tile.clickData[1] = {"State","Open","Close"}
	tile.clickFunctions["State"] = function(tile,choice)
		local state = "Off";
		if(choice == "Open")then state = "On" end
		tile.dataObject.State.initialState = state
		tile.initialState = state
		tile:Reset()
	end
	tile.reverseFunctions["State"] = function(tile)
		local choice = "Close"
		if(tile.initialState == "On")then choice = "Open" end
		return choice
	end

	tile:AddUpdate(Update)
end

RightClickCodes.Toggle = function(tile)
	---This code initializes the right click for this tile
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}
	tile.clickData[1] = {"State","Open","Close"}
	tile.clickData[2] = {"Sensor","On","Off"}
	tile.clickData[3] = {"Range","Small","Medium","Large"}

	-------------The state subtitle
	tile.clickFunctions["State"] = function(tile,choice)
		local state = "Off";
		if(choice == "Open")then state = "On" end
		tile.dataObject.State.initialState = state
		tile.initialState = state
		tile:Reset()
	end
	tile.reverseFunctions["State"] = function(tile)
		local choice = "Close"
		if(tile.initialState == "On")then choice = "Open" end
		return choice
	end

	-------The Sensor subtitle
	tile.clickFunctions["Sensor"] = function(tile,choice)
		local state = false;
		if(choice == "On")then state = true end
		tile.dataObject.State.Sensor = state
		tile.Sensor = state
		---disable/enable the sensor
		tile:SetupSensor()
	end
	tile.reverseFunctions["Sensor"] = function(tile)
		local choice = "Off"
		if(tile.Sensor == true)then choice = "On" end
		return choice
	end

	-------The Range subtitle
	tile.clickFunctions["Range"] = function(tile,choice)
		local state = "Small";
		if(choice == "Medium")then state = "Med" end
		if(choice == "Large")then state = "Large" end
		tile.dataObject.State.Range = state
		tile.Range = state
		---change sensor size
		tile:SetupSensor()
	end
	tile.reverseFunctions["Range"] = function(tile)
		local choice = "Small"
		if(tile.Range == "Med")then choice = "Medium" end
		if(tile.Range == "Large")then choice = "Large" end
		return choice
	end

	tile:AddUpdate(Update)
end

RightClickCodes.Magnet = function(tile)
	---This code initializes the right click for this tile
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}
	tile.clickData[1] = {"State","On","Off"}
	tile.clickData[2] = {"Mode","Attract","Repulse"}

	tile.clickFunctions["State"] = function(tile,choice)
		local state = choice;
		tile.dataObject.State.initialState = state
		tile.initialState = state
		tile:Reset()
	end
	tile.reverseFunctions["State"] = function(tile)
		return tile.initialState
	end

	tile.clickFunctions["Mode"] = function(tile,choice)
		tile.mode = choice;
		tile.dataObject.State.mode = choice;
		tile:Reset();
	end
	tile.reverseFunctions["Mode"] = function(tile)
		return tile.mode;
	end

	tile:AddUpdate(Update)
end

RightClickCodes.Light = function(tile)
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}
	tile.clickData[1] = {"State","On","Off"}
	tile.clickData[2] = {"Effect","None","Flicker"}
	tile.clickData[3] = {"Color","Select Color.."}

	----------------State
	tile.clickFunctions["State"] = function(tile,choice)
		tile.dataObject.State.initialState = choice
		tile.initialState = choice
		tile:Reset()
	end
	tile.reverseFunctions["State"] = function(tile)
		return tile.initialState
	end
	-----------End State

	-------Effect
	tile.clickFunctions["Effect"] = function(tile,choice)
		tile.dataObject.State.Effect = choice;
		tile.effect = choice;
	end
	tile.reverseFunctions["Effect"] = function(tile)
		return tile.effect
	end
	------End effect

	---Color
	tile.clickFunctions["Color"] = function(tile,choice)
		local originalColor = {tile.customColor[1],tile.customColor[2],tile.customColor[3]}

		local OnColorChanged = function(R,G,B)
			tile.customColor = {R/255,G/255,B/255}
			print(R,G,B)
		end
		local OnClose = function()
			--close without saving, reset to original color
			print("Closed without saving")
			tile.customColor = originalColor;
		end
		local OnOkay = function()
			--save the color
			print("Saving")
		end

		---opens a color picker window
		MakeColorPicker(OnColorChanged,OnClose,OnOkay,{tile.customColor[1] * 255,tile.customColor[2] * 255,tile.customColor[3] * 255})
	end

	tile.reverseFunctions["Color"] = function(tile)
		return 1;--isn't meant to do anything
	end
	---------End Color


	tile:AddUpdate(Update)
end

RightClickCodes.Checkpoint = function(tile)
	tile.clickData = {};
	tile.clickFunctions = {}
	tile.reverseFunctions = {}
	tile.clickData[1] = {"Identifier","Set ID.."}
	if(tile.dataObject.State.id ~= nil)then 
		tile.clickData[1][2] = tile.dataObject.State.id
	end 
	tile.clickFunctions["Identifier"] = function(tile,choice)
		local window = MakeWindow(280,190,"Identifier",quads);
		local t = tile.dataObject.State.id;
		if(t == nil)then t = "Untitled" end
		local input = MakeInput(150,65,window,t)
		local ok = MakeOkButton(100,window,quads)
		ok.OnClick = function(self)
			local id = input:GetText()
			tile.dataObject.State.id = id;
			tile.clickData[1][2] = tile.dataObject.State.id
			window:Destroy()
		end
	end
	tile.reverseFunctions["Identifier"] = function(tile)
		
	end

	tile:AddUpdate(Update)
end


----Entities

RightClickCodes.SpawnJoe = function(entity)
	---This code initializes the right click for this entity
	entity.clickData = {};
	entity.clickFunctions = {}
	entity.reverseFunctions = {}


	entity.clickData[1] = {"Spawn","Joe 1","Joe 2","Joe 3","Joe 4"}
	entity.clickFunctions["Spawn"] = function(entity,choice)
		local num = choice:gsub("Joe ","")
		num = tonumber(num)
		entity.dataObject.joeNum = num;
	end
	entity.reverseFunctions["Spawn"] = function(entity)
		local num = entity.dataObject.joeNum;
		if(num == nil)then num = 1 end
		local choice = "Joe " .. num;

		return choice
	end

	entity:AddUpdate(Update)
end

if _ForgeEngine then RightClickCodes = _ForgeEngine:embedRCC(RightClickCodes) end

return RightClickCodes;