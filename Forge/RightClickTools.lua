local RightClickTools = {};
local UIObject = require("Engine/UIObject");
local currentLevel;
local quads;

function RightClickTools:init(level)
	currentLevel = level
	if(quads == nil)then 
		local imagemanip = require("Engine/ImageManipulator");
		local o = imagemanip:StripImages("EditorUISheet",nil,nil,nil,{});
		quads = o.quadArray;
	end
end

--Useful local window functions
RightClickTools.MakeWindow = function(w,h,name,quads)
	--Makes a window in the centre of the screen
	local window = UIObject:newObject(960 - w/2,350 - h/2,"EditorUISheet","frame",quads["FineJeez_N"])
	window:MakeCustom(w,h,quads,name)
	window:SetModal(true)
	window.onClose = function() _Screen.focus = "game" end
	return window
end

RightClickTools.MakeInput = function(width,Y,window,defaultValue,offX)
	--makes an input field and centers it in the window
	local w = window.width
	local X = w/2 -width/2
	if(offX == nil)then offX = 0 end
	local t = UIObject:newObject(X + offX,Y,"EditorUISheet","textinput")
	t:SetParent(window)
	t.Owidth = width
	t.width = t.Owidth
	t:Rescale()
	t:SetText(defaultValue)
	
	return t;
end

RightClickTools.MakeOkButton = function(Y,window,quads,offX,name)
	local w = window.width
	if(offX == nil)then offX = 0 end
	local ok = UIObject:newObject(w/2,Y,"EditorUISheet","button")
	ok:SetParent(window)
	if(name == nil)then name = "OkLong" end
	ok:SetButtonQuad(name,quads)
	local a,b,wid,h = ok.quad:getViewport()
	ok:SetMyX(w/2 - wid/2 + offX)

	ok.RightClickTools.Update = function(self)
		if(_KeyTap["return"] or _cKeyTap["return"])then 
			self:OnClick()
		end
	end

	return ok;
end

RightClickTools.MakeText = function(Y,Text,color,font,window,offX)
	---Makes text that is centered in the window
	local w = window.width 
	local textWidth = _Fonts[font]:getWidth(Text)
	if(offX == nil)then offX = 0 end
	local text = UIObject:newObject(w/2 - textWidth/2 + offX,Y,nil,"text")
	text:SetParent(window)
	text:SetText(Text,color)
	text:SetFont(_Fonts[font])
	text.clickThrough = true
	return text
end

RightClickTools.MakeColorPicker = function(onColorChanged,OnClose,OnOkay,defaultColor)
	local window = RightClickTools.MakeWindow(400,500,"Color Picker",quads);
	local Okay = RightClickTools.MakeOkButton(400,window,quads)
	local saved = false;
	Okay.OnClick = function(self)
		saved = true;
		window:Destroy()
		OnOkay()
	end

	RightClickTools.MakeText(100,"Red",{130,140,140,255},"AlteHaas15",window)
	RightClickTools.MakeText(200,"Green",{130,140,140,255},"AlteHaas15",window)
	RightClickTools.MakeText(300,"Blue",{130,140,140,255},"AlteHaas15",window)

	local rSlider = UIObject:newObject(130,130,"","slider");
	rSlider:SetWidth(120)
	rSlider:SetMinMax(0, 255)
	rSlider:SetParent(window)
	rSlider:SetValue(defaultColor[1])

	local gSlider = UIObject:newObject(130,230,"","slider");
	gSlider:SetWidth(120)
	gSlider:SetMinMax(0, 255)
	gSlider:SetParent(window)
	gSlider:SetValue(defaultColor[2])

	local bSlider = UIObject:newObject(130,330,"","slider");
	bSlider:SetWidth(120)
	bSlider:SetMinMax(0, 255)
	bSlider:SetParent(window)
	bSlider:SetValue(defaultColor[3])


	rSlider.OnValueChanged = function(object)
		local r,g,b = rSlider:GetValue(),gSlider:GetValue(),bSlider:GetValue()
		onColorChanged(r,g,b)
	end
	bSlider.OnValueChanged = rSlider.OnValueChanged;
	gSlider.OnValueChanged = rSlider.OnValueChanged;

	--has a special close function to destroy the mark
	window.onClose = function(self)
		_Screen.focus = "game"
		if(OnClose ~= nil and not saved)then OnClose() end
	end
end

RightClickTools.createMenu = function(tile)
	local f = (1920/_Screen.width)
	local menu = UIObject:newObject(_Mouse.x * f,_Mouse.y * f,"","panel")
	tile.rightMenu = menu
	tile.innerButtons = {}
	local n = 0;
	for i=1,#tile.clickData do 
		local d = tile.clickData[i]
		local title = d[1];
		local frame = UIObject:newObject(0,n * 30,"EditorUISheet","frame",quads["ContextMenuTitle"])
		frame:SetParent(menu); 
		frame.displayText = title
		frame.displayTextColor = {100,115,123,255}
		n = n + 1
		local selectedName = tile.reverseFunctions[title](tile);
		tile.innerButtons[i] = {}
		for j=2,#d do 
			local button = UIObject:newObject(0,n * 30,"EditorUISheet","button")
			button:SetButtonQuad("ContextMenu",quads)
			button.activeQuad = quads["ContextMenu_O"]
			button.sectorButtons = tile.innerButtons[i];
			button.titleName = title
			button.OnClick = function(self)
				for i=1,#self.sectorButtons do 
					self.sectorButtons[i].Active = false
				end
				self.Active = true
				---run function
				local func = tile.clickFunctions[self.titleName];
				if(tile.OnRightclickRun ~= nil)then 
					tile.OnRightclickRun(func,self.titleName,self.displayText)
				end
				func(tile,self.displayText)
				tile.rightClicked = false
			end
			if(d[j] == selectedName)then button.Active = true end
			button:SetParent(menu);
			button.displayText = d[j]
			button.displayTextColor = {78,102,74,255}
			tile.innerButtons[i][#tile.innerButtons[i]+1] = button
			n = n + 1
		end
	end
end

RightClickTools.Update = function(self)----RightClickTools.Update function
	local menu = self.rightMenu
	if(self.rightClicked)then ----------------If right clicking
		if(menu == nil)then 
			RightClickTools.createMenu(self)
		end
		if(_mouseUp["l"] or _mouseUp["r"] or _mouseTap["m"])then 
			local touched = false
			for k, v in ipairs(menu.children) do
				if(v.hover)then touched = true; break end
			end
			if(not touched)then self.rightClicked = false end
		end

		for i=1,#self.innerButtons do 
			for j=1,#self.innerButtons[i]do
				local b = self.innerButtons[i][j];
				if(b.hover or b.Active)then 
					b.displayTextColor = {255,255,255,255}
				else 
					b.displayTextColor = {78,102,74,255}
				end
			end
		end
	end -----------------------------End if right clicking

	if(self.rightClicked ~= true)then 
		if(menu ~= nil)then 
			menu:Destroy()
			self.rightMenu = nil
		end
	end
end -------------End RightClickTools.Update function

return RightClickTools;