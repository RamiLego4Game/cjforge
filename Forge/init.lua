_Forge, _ForgeEngine = { version = 0.1 }, {};

_ForgeEngine.rcclist = {};

--[[---------------------------------------------------------
	- func: registerRightClick(function,modfilename)
	- desc: registers right click menu code at level editor
--]]---------------------------------------------------------
function _Forge:registerRightClick(func,filename)
  if _ForgeEngine.rcclist[filename] == nil then _ForgeEngine.rcclist[filename] = func end
end

--[[---------------------------------------------------------
	- Engine Functions: Those functions are used by forge
--]]---------------------------------------------------------

function _ForgeEngine:initCores()
  local files = love.filesystem.getDirectoryItems("Mods/Core/");
  for k, file in ipairs(files) do
      if love.filesystem.isFile("Mods/Core/"..file) then
        local code = love.filesystem.load("Mods/Core/"..file)();
      end
  end
end

function _ForgeEngine:embedRCC(RCC)
  for filename,func in pairs(_ForgeEngine.rcclist) do
    RCC[filename] = func;
  end
  return RCC;
end