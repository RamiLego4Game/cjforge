--[[
 * Copyright (c) 2012-2014 Those Awesome Guys (TAG)
 *
 * You may NOT use this software for any commercial 
 * purpose, nor can you alter and redistribute it as your own.
 * Any credit must be attributed to Those Awesome Guys (TAG)
 * 
 * You are allowed and even encouraged to mod(ify) the game 
 * but all alterations must be specificed as a mod of the game
 * Concerned Joe, and not claim that Concerned Joe itself is a game
 * of your creation.
 *
 *  This notice may not be removed or altered from any source distribution.
 --]]


local Menu = {};
local loader;

local h = {};--my little holder
local buttons = {};
local StateButtons = {};--holds a list of the buttons in each state
local StateLists = {};--holds a list of the list in each state (if exists)
local StateConfirm = {};--holds the confirm button in each state (if exists)
local StateBack = {};---holds the back button in each state (if exists)
local StateAlt = {};---hold the alt button in each state (if exists)
local AltInput = {};
local listOfObjects = {};
local menuData; --the json data of MainMenuSheet
local State = "Main";--the state to set all the objects to when created
local ObjStates = {};--holds states
local StateData = {};--holds data like whether to show top and bottom strips
local cursor = love.mouse.getSystemCursor("hand")
local IsList = false;
local activeInput = "Mouse";--can be "Keyboard" or "Xbox" or "PS3"
local prompts = {};
local selectedNum = 0;
local StateList = {};--keeps track of previous states
local openWindow = {isDestroyed=true};
--[[
current attributes (and their defaults)
top = false //shows or hides the top strip
bottom = false //shows or hirdes the bottom strip
topHeight = 150 //the height of the top strip
]]--

--libraries
local gameobj = require("Engine/GameObject")
local UIObject = require("Engine/UIObject");
local imgmanip = require("Engine/ImageManipulator")

local dynamicCreate = function(refName,x,y,ImageName,t,init)
	--waits for the object to be loaded to create it
	if(t == nil)then t = "UI" end

	local holder = {};
	holder.ready = false;
	holder.data = {x,y,ImageName,t,refName,init};

	listOfObjects[#listOfObjects+1] = holder;

	return holder;
end

local createObjects = function()
	--wait for objects to load to create them
	for i=1,#listOfObjects do 
		local obj = listOfObjects[i];
		if(_Images[obj.data[3]])then 
			local t = obj.data[4];
			if(t == "UI")then h[obj.data[5]] = gameobj:newUIObject(obj.data[1],obj.data[2],obj.data[3]) end
			if(obj.data[6])then obj.data[6](); end
			table.remove(listOfObjects,i); break;
		end
	end
end

local bgCounter = 0;
local currentBg = 1;

local changingBackground = function(dt)
	bgCounter = bgCounter + dt;
	local bg2 = h["Background2"];
	local bg = h["Background"]
	if(bg)then 
		if(bg.alpha < 1)then bg.alpha = bg.alpha + dt end 
	end

	if(bgCounter > 3)then 
		bgCounter = 0;
		if(bg and _Images["MenuBg" .. (currentBg+1)])then 
			bg2.sprite = bg.sprite;
			bg.sprite = _Images["MenuBg" .. (currentBg+1)]
			bg.alpha = 0;
			currentBg = currentBg +1;
			if(currentBg == 4)then currentBg = 0 end
		end
	end
end


function Menu:MakeButton(ButtonName,X,Y,onUp,Type)
	local text = UIObject:newObject(X,Y,nil,"text")
	local color = {220,220,220,255}
	text:SetText(ButtonName,color);
	text:SetFont(GetFont("AlteHaas.ttf",40));
	text:SetState(State)
	text.onUp = onUp;
	if(onUp == nil)then text.inActive = true end
	text.mouseHover = false;
	text.shadow = true;
	text.pressedInside = false;

	text.Update = function(s)
		if(s.onUp == nil)then s.alpha = .2; return end
		local x,y = s.x,s.y;
		local w,h = s:GetWidth(),s:GetHeight();
		local f = 1920/_Screen.width;
		local fY = 1080/_Screen.height;
		local mx,my = _Mouse.x*f,_Mouse.y*f;
		local diff = _Screen.height * f - _Screen.height * fY;
		y = y + diff / 2;

		local padding = 0;
		local inside = false;
		if(love.mouse.isVisible() == true and mx > (x-padding) and mx < (x+w+padding) and my > (y-padding) and my < (y+h+padding))then 
			inside = true;
		end

		if(inside or s.selected)then 
			s:SetColor({255,255,255,255})
		else 
			s:SetColor(color)
		end
		s.mouseHover = inside;

		if(inside and _mouseTap["l"])then 
			s.pressedInside = true 
		end

		if(s.pressedInside and _mouseUp["l"])then 
			s.onUp()
		end
		if(_mouseUp["l"])then s.pressedInside = false end

	end
	buttons[#buttons+1] = text;

	if(onUp and IsList)then --if not disabled
		if(StateButtons[State] == nil)then StateButtons[State] = {} end
		StateButtons[State][#StateButtons[State]+1] = text;

		text.Draw = function(self)
			if(self.mouseHover or self.selected)then 
				love.graphics.setColor(255,255,255,55)
				love.graphics.rectangle("fill",self.x-10,self.y,350,50)
				love.graphics.setColor(255,255,255,255)
			end
		end
	end

	local promptCheck = function(self)
		local quadName = "Xbox_Confirm";
		if(self.confirm)then 
			if(activeInput == "Xbox")then quadName = "Xbox_Confirm" end
			if(activeInput == "Keyboard")then quadName = "Keyboard_Confirm" end
			if(activeInput == "PS3")then quadName = "PS_Confirm" end 
		end

		if(self.back)then 
			if(activeInput == "Xbox")then quadName = "Xbox_Back" end
			if(activeInput == "Keyboard")then quadName = "Keyboard_Back" end
			if(activeInput == "PS3")then quadName = "PS_Back" end 
		end

		if(self.alt)then 
			if(activeInput == "Xbox")then quadName = "Xbox_Alt" end
			if(activeInput == "Keyboard")then quadName = "Keyboard_Alt" end
			if(activeInput == "PS3")then quadName = "PS_Alt" end 
		end

		if(activeInput == "Mouse")then self.visible = false else self.visible = true end
		self:ChangeQuad(menuData.quadArray[quadName])

	end

	if(Type == "confirm")then 
		StateConfirm[State] = text;
		local p = Menu:MakeObject(X-40,Y+25,"Xbox_Confirm")
		p.confirm = true;
		p:AddUpdate(promptCheck)
		prompts[#prompts+1] = p;
	end
	if(Type == "back")then 
		StateBack[State] = text;
		local p = Menu:MakeObject(X-40,Y+25,"Xbox_Back")
		p.back = true;
		p:AddUpdate(promptCheck)
		prompts[#prompts+1] = p;
	end
	if(Type == "alt")then 
		StateAlt[State] = text;
		local p = Menu:MakeObject(X-40,Y+25,"Xbox_Back")
		p.alt = true;
		p:AddUpdate(promptCheck)
		prompts[#prompts+1] = p;
	end

	return text;
end

function Menu:MakeText(X,Y,txt,size)
	if(size == nil)then size = 40 end
	local text = UIObject:newObject(X,Y,nil,"text")
	text:SetText(txt,{255,255,255,255});
	text:SetFont(GetFont("AlteHaas.ttf",size));
	text:SetState(State)
	text.shadow = true;

	return text;
end

function Menu:MakeObject(X,Y,name)
	local obj = gameobj:newUIObject(X,Y,"MainMenuSheet");
	obj:ChangeQuad(menuData.quadArray[name])
	obj.layer = 3;
	obj.offSetX = 0; obj.offSetY = 0;

	--set it to the current state
	if(ObjStates[State] == nil)then ObjStates[State] = {} end
	ObjStates[State][#ObjStates[State]+1] = obj;

	return obj;
end

function Menu:MakeList(quads)--mainly for options menu's
	local container = UIObject:newObject(0,-300,"EditorUISheet","frame",quads["FineJeez_N"]);
	container:SetState(State)
	local list = UIObject:newObject(160,250+300,"EditorUISheet","list")
	list.Owidth = 1700; list.Oheight = 550
	list:SetParent(container)
	list:Rescale()
	list:DisableButtons()
	list.barX = -40 - list.Owidth;
	list.barY = 10;
	list.customBarHeight = 540;
	list:SetState(State)
	list.rowArray = {};

	local menuQuads = menuData.quadArray;

	list.Update = function(self)
		for i=1,#self.rowArray do 
			local row = self.rowArray[i];
			local f = 1920/_Screen.width;
			local col = loveframes.util.BoundingBox3(_Mouse.x * f, row.x, _Mouse.y * f, row.y, 1, 1920, 1, 60)
			row.myhover = false;
			if(col)then row.myhover = true end
		end
		if(self.SetDefaults)then self:SetDefaults() end--to set the list's defaults once it appears on screen
	end

	list.AddMyItem = function(self,bigText,optionArray,description,default,sliderData,buttonData)
		local defaultNum = 1;
		if(optionArray ~= nil)then 
			for i=1,#optionArray do if(optionArray[i] == default)then defaultNum = i; break end end
		end

		local panel = UIObject:newObject(0,0,"EditorUISheet","panel");
		panel:SetParent(self);
		self.rowArray[#self.rowArray+1] = panel;
		panel.index = #self.rowArray;
		panel:SetState(State);
		panel.Oheight = 70; panel:Rescale()
		panel.Draw = function(self)
			if(panel.disabled)then return end
			if(self.selected and activeInput ~= "Mouse" or self.myhover and activeInput == "Mouse")then 
				love.graphics.setColor(255,255,255,55)
				love.graphics.rectangle("fill",0,self.y-3,1920-50,50)
				love.graphics.setColor(255,255,255,255)
			end
		end
		panel.Update = function(self)
			if(self.selected)then
				if(self.arrowRight and self.arrowLeft)then  
					if(AltInput["right"])then self.arrowRight:OnClick() end
					if(AltInput["left"])then self.arrowLeft:OnClick() end
				end
				if(self.slider)then 
					local max = self.slider:GetMax()
					local min = self.slider:GetMin()
					local range = max - min;
					local value = self.slider:GetValue();

					if(AltInput["right"])then 
						local newvalue = value + .1 * range;
						if(newvalue > max)then newvalue = max end
						self.slider:SetValue(newvalue); 
					end
					if(AltInput["left"])then 
						local newvalue = value - .1 * range;
						if(newvalue < min)then newvalue = min end
						self.slider:SetValue(newvalue); 
					end
				end
			end
		end

		local text = Menu:MakeText(20,0,bigText,30);
		text:SetParent(panel); 
		panel.mainText = text;

		local Arrows = true;
		if(sliderData ~= nil or buttonData ~= nil)then Arrows = false end
		local Y = 7;
		local X = 380;

		if(Arrows)then 
			local option = Menu:MakeText(200+X,0,optionArray[defaultNum],30);
			option.staticx = 200+X - option:GetWidth()/2;
			option:SetParent(panel);
			option.index = defaultNum;
			option.originalX = X;
			option.max = #optionArray;

			-----Arrows
			local calculateLimits = function(arrow)
				local enable = true;
				if(arrow.optionText.index == 1 and arrow.left)then 
					enable = false;
				end
				if(arrow.optionText.index == arrow.optionText.max and arrow.right)then 
					enable = false;
				end

				if(enable)then 
					arrow.normalQuad = menuQuads["Arrow_N"];
					arrow.overQuad = menuQuads["Arrow_O"];
					arrow.downQuad = menuQuads["Arrow_O"];
					arrow.quad = arrow.normalQuad
				else 
					arrow.normalQuad = menuQuads["Arrow_I"];
					arrow.overQuad = menuQuads["Arrow_I"];
					arrow.downQuad = menuQuads["Arrow_I"];
					arrow.quad = arrow.normalQuad
				end
			end
			local arrowClick = function(s)
				local t = s.optionText;
				if(s.left)then 
					if(t.index > 1)then t.index = t.index - 1 end
				end
				if(s.right)then 
					if(t.index < t.max)then t.index = t.index + 1 end
				end
				t:SetText(s.optionArray[t.index]);
				t.staticx = 200+t.originalX - t:GetWidth()/2;
				calculateLimits(s)
				calculateLimits(s.other)
			end

			local arrowUpdate = function(s)
				s.mouseHover = s.hover;
			end

			panel.setDefault = function(self,name)
				local t = self.optionText
				local s = self.arrowLeft
				local index = t.index;
				for i=1,#s.optionArray do if(s.optionArray[i] == name)then index = i; break end end
				t.index = index;
				t:SetText(s.optionArray[t.index]);
				t.staticx = 200+X - t:GetWidth()/2;
				calculateLimits(s)
				calculateLimits(s.other)
			end

			local arrowWidth = 40;
			local arrowLeft = UIObject:newObject(380,0,"MainMenuSheet","button",menuQuads["Arrow_N"]);
			arrowLeft:SetState(State);
			arrowLeft:SetParent(panel);
			arrowLeft:SetButtonQuad("Arrow",menuQuads);
			arrowLeft.optionArray = optionArray;
			arrowLeft.optionText = option;
			arrowLeft.OnClick = arrowClick;
			arrowLeft.left = true;
			arrowLeft.scaleX = -1; arrowLeft.offSetX = arrowLeft:GetWidth() * _TEXTURESCALES[GetImageQuality("MainMenuSheet")]
			calculateLimits(arrowLeft);
			buttons[#buttons+1] = arrowLeft;
			arrowLeft.Update = arrowUpdate;

			local arrowRight = UIObject:newObject(730,0,"MainMenuSheet","button",menuQuads["Arrow_N"]);
			arrowRight:SetState(State);
			arrowRight:SetParent(panel);
			arrowRight:SetButtonQuad("Arrow",menuQuads);
			arrowRight.optionArray = optionArray;
			arrowRight.optionText = option;
			arrowRight.right = true;
			arrowRight.OnClick = arrowClick;
			calculateLimits(arrowRight);
			buttons[#buttons+1] = arrowRight;
			arrowRight.Update = arrowUpdate;

			arrowLeft.other = arrowRight;
			arrowRight.other = arrowLeft;
			panel.arrowRight = arrowRight;
			panel.arrowLeft = arrowLeft;
			panel.optionText = option;
			---End arrows

			if(#optionArray == 1)then --if only one item, hide arrows
				arrowLeft.visible = false;
				arrowRight.visible = false;
			end
		end

		if(sliderData ~= nil)then 
			---Add the slider
			local sliderWidth = 300;
			local Slider = UIObject:newObject(200+X - (sliderWidth/2),0,"","slider");
			Slider:SetWidth(sliderWidth)
			Slider:SetMinMax(sliderData[1], sliderData[2])
			Slider:SetParent(panel)
			Slider:SetValue(default)
			Slider.OnValueChanged = function(self)
				print(self:GetValue())
			end
			panel.slider = Slider;
		end

		if(buttonData ~= nil)then 
			local button = Menu:MakeButton(buttonData[2],X,0,buttonData[1])
			button:SetFont(GetFont("AlteHaas.ttf",30));
			button.staticx = 200+X - (button:GetWidth()/2)
			button:SetParent(panel)
			panel.button = button;
		end

		local desc = Menu:MakeText(420+X,Y,description,20);
		desc:SetParent(panel);
		desc:SetColor({150,150,150,255})
		panel.desc = desc;

	end

	list.SelectRow = function(self,row)
		for i=1,#self.rowArray do 
			if(self.rowArray[i] == row)then self.rowArray[i].selected = true; else self.rowArray[i].selected = false; end
		end

		if(row ~= nil)then 
			local scrollbar = list:GetScrollBar();
			if(scrollbar ~= false and row.y > self.y+self.height)then				
				scrollbar:Scroll(50)
			end
			if(scrollbar ~= false and row.y < self.y)then 
				scrollbar:Scroll(-50)
			end
		end
	end

	list.Disable = function(self,row)
		local text = row.mainText;
		local color = {55,55,55,255}
		text:SetText(text.text,color)

		row.disabled = true;
		if(row.arrowLeft ~= nil)then 
			row.arrowLeft:SetEnabled(false)
			row.arrowRight:SetEnabled(false)
			text = row.optionText;
			text:SetText(text.text,color)
		end
		if(row.button ~= nil)then 
			row.button.onUp = nil;
			row.button.inActive = true;
		end
		if(row.slider ~= nil)then 
			row.slider:SetEnabled(false)
		end

		text = row.desc;
		--text:SetText(text.text,color)
	end

	list.AddBreak = function(self,breakText)
		local panel = UIObject:newObject(0,0,"EditorUISheet","panel");
		panel:SetParent(self);
		panel:SetState(State);
		panel.Oheight = 70; panel:Rescale()

		local text = Menu:MakeText(0,0,"ÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆ " .. breakText .. " ÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆÆ",30);
		text:SetParent(panel); 
		text:SetColor({120,120,120,255})
	end



	return list;
end


local initMenu = false;
local finishFunc = nil;

local hideButtons = function()
	for i=1,#buttons do 
		buttons[i].x = -1000;
	end
end

--[[
=============================================
=============================================
===========The Menu States/Pages=============
=============================================
=============================================
]]--

local currentLevel = ""
local currentList = "";
local doubleClickTime = 0;

local SaveStartup = function(save)
	--Save all these settings in the startup
	local Text = love.filesystem.read("startup.ini");
	Text = RemoveGhosts(Text);
	local startupText = split(Text,"\n")
	if(#startupText > 0)then -----if startup.ini exists
		local newText = "";

		for i=1,#startupText do 
			local line = startupText[i]
			if(i < #startupText)then line = line:sub(1,line:len()) end

			if(line:find("resolution") and save["Resolution"] ~= nil)then 
				line = save["Resolution"]
			end
			if(line:find("antialiasing") and save["FSAA"] ~= nil)then 
				line = save["FSAA"]
			end
			if(line:find("vsync") and save["Vsync"] ~= nil)then 
				line = save["Vsync"]
			end
			if(line:find("wmode") and save["Wmode"] ~= nil)then 
				line = save["Wmode"]
			end
			if(line:find("fpscap") and save["Cap"] ~= nil)then 
				line = save["Cap"]
			end
			if(line:find("quality") and save["Quality"] ~= nil)then 
				line = save["Quality"]
			end
			if(line:find("monitor") and save["Monitor"] ~= nil)then 
				line = save["Monitor"]
			end
			if(line:find("mouselock") and save["Mouselock"] ~= nil)then 
				line = save["Mouselock"]
			end
			if(line:find("postprocess") and save["PostProcess"] ~= nil)then 
				line = save["PostProcess"]
			end

			newText = newText .. line;
			if(i < #startupText)then newText = newText .. "\n" end
		end

		--Write to the startup.ini file
		local file = io.open(_Dir .. "startup.ini","w");
		local m,e = file:write(newText);
		file:close();
		if(not m)then error(e) end
		--Not sure if this works on Mac and Linux ^

	end ---------------------End if startup.ini exists
end

local makeMenuObjects = function()
	---Some initilizations 
	menuData = imgmanip:StripImages("MainMenuSheet",0,0,"UI",{});
	local q = imgmanip:StripImages("EditorUISheet",0,0,"UI",{});
	local EditorQuads = q.quadArray;
	loveframes.util.SetEditorSheetQuads(EditorQuads)

	--=======Main=======--
	State = "Main";
	
	h["Title"] = Menu:MakeObject(125,120,"MenuTitle")
  if _Forge then
    h["Version"] = Menu:MakeText(135,253,"Alpha Version " .. _Version .. " - CJForge Version " .. _Forge.version,25)
  else
    h["Version"] = Menu:MakeText(135,253,"Alpha Version " .. _Version,25)
  end
	h["Version"]:SetText(h["Version"].text,{255,255,255,155})
	local playFunc = function() 	
		Menu:SetState("LoadLevel")
	end
	local playlistFunc = function()
		Menu:SetState("LoadList");
	end
	local partyFunc = function()
		Menu:SetState("Party")
	end
	local editorFunc = function() 
		finishFunc = function()Menu:starteditor() end; 
		Menu:LoadAll()
		Menu:SetState("Loading")
	end
	local optionsFunc = function() Menu:SetState("Options") end
  local cjforgeFunc = function()
		openURL("https://bitbucket.org/RamiLego4Game/cjforge/")
	end
	local reportFunc = function()
		openURL("https://bitbucket.org/xelu/concerned-joe-alpha/issues/new")
	end
	local quitFunc = function() Menu:SetState("Quit") end

	local X = 150;
	local Y = 580;
	IsList = true
	Menu:MakeButton("Story",X,Y-55);
	Menu:MakeButton("Party Mode",X,Y,partyFunc);
	Menu:MakeButton("Load Level",X,Y+55*1,playFunc);
	Menu:MakeButton("Playlists",X,Y+55*2,playlistFunc);
	Menu:MakeButton("Level Editor",X,Y+55*3,editorFunc);
	Menu:MakeButton("Options",X,Y+55*4,optionsFunc)
	IsList = false
  if _Forge then Menu:MakeButton("CJForge",1540,850,cjforgeFunc) end
	Menu:MakeButton("Report a bug",1500,925,reportFunc,"alt");
	Menu:MakeButton("Quit",X,925,quitFunc,"back")

	--=======Options=======--
	State = "Options";
	
	Menu:MakeButton("Back",X,925,function() Menu:SetState("Main") end,"back")
	h["OptionsTitle"] = Menu:MakeText(X,100,"Options",65)

	local videoFunc = function() Menu:SetState("Video Options") end 
	local audioFunc = function() Menu:SetState("Audio Options") end 
	local controlFunc = function() Menu:SetState("Controls Options") end

	local X = 150;
	local Y = 580;
	IsList = true
	Menu:MakeButton("Video",X,Y,videoFunc);
	Menu:MakeButton("Audio",X,Y+55*1,audioFunc);
	Menu:MakeButton("Gameplay",X,Y+55*2);
	Menu:MakeButton("Controls",X,Y+55*3,controlFunc);
  Menu:MakeButton("CJForge",X,Y+55*4);
	IsList = false

	--=======Video Options=======--
	State = "Video Options";

	local BuiltInRes = {"960x540","1280x720","1366x768","1600x900","1920x1080","Native"};
	local monitorIDArray = {};
	local maxMonitors = love.window.getDisplayCount()
	for i=1,maxMonitors do 
		monitorIDArray[i] = tostring(i);
	end
	if(#monitorIDArray == 1)then monitorIDArray = {"Only one monitor detected"} end

	local list = Menu:MakeList(EditorQuads);
	h["VideoList"] = list;
	StateLists[State] = list;
	list:AddMyItem("Resolution",BuiltInRes,"Changes the resolution of the game.",defaultResolution);
	list:AddMyItem("Display",{"Windowed","Borderless","Fullscreen"},"Determines the way the game is displayed on your screen.",defaultDisplay);
	list:AddMyItem("Monitor",monitorIDArray,"Changes the monitor the game is displayed on.",defaultMonitor);
	list:AddMyItem("Vsync",{"Off","On"},"Vsync removes screen tearing and limits the frame rate to your monitor's refresh rate.",defaultVsync);
	list:AddBreak("Advanced")
	list:AddMyItem("Antialiasing",{"Off","FSAAx2","FSAAx4","FSAAx8","FSAAx16"},"Antialiasing smoothens rough edges and makes everything look sharper.",defaultFSAA);
	list:AddMyItem("FPS cap",{"60","120","200","Unlimited"},"FPS cap limits the maximum amount of frames per second.",defaultFPS);
	list:AddMyItem("Texture quality",{"Low","Medium","High"},"Texture quality defines the sharpness of the graphics.",defaultQuality);
	list:AddMyItem("Post Processing",{"Off","On"},"Post processing effects are things like color correction, depth of field and the death effect.",defaultPostProccessing);
	list:AddMyItem("Out of focus",{"Pause game","Keep game running","Run at low fps"},"This setting determines the behaviour of the game while out of focus.");
	list:Disable(list.rowArray[#list.rowArray])

	list.SetDefaults = function(self)
		if(self.done ~= true)then 
			self.done = true;
			------get the defaults of all the options
			local width,height,flags = love.window.getMode();

			local defaultResolution = width .. "x" .. height;
			local defaultDisplay = "Windowed";
			local defaultVsync = "Off";
			local defaultFSAA = "Off";
			local defaultFPS = "Unlimited";
			local defaultQuality = _TEXTUREQUALITY;
			local defaultMipmap = "High"
			local defaultMonitor = tostring(flags["display"]);
			local BuiltInRes = {"960x540","1280x720","1366x768","1600x900","1920x1080","Native"};
			local monitorIDArray = {};
			local found = false;
			for i=1,#BuiltInRes do 
				if(BuiltInRes[i] == defaultResolution)then found = true; break end
			end
			local maxMonitors = love.window.getDisplayCount()
			for i=1,maxMonitors do 
				monitorIDArray[i] = tostring(i);
			end
			if(#monitorIDArray == 1)then monitorIDArray = {"Only one monitor detected"} end

			if(not found)then defaultResolution = "Native" end
			if(flags["borderless"])then defaultDisplay = "Borderless" end
			if(flags["fullscreen"])then defaultDisplay = "Fullscreen" end
			if(flags["vsync"])then defaultVsync = "On" end
			if(tonumber(flags["fsaa"]) > 0)then defaultFSAA = "FSAAx" .. flags["fsaa"] end
			if(_FPScap > 0)then defaultFPS = tostring(_FPScap) end
			if(_Mipmap ~= false)then 
				if(_Mipmap == -1)then defaultMipmap = "Low" end
				if(_Mipmap == -0.1)then defaultMipmap = "Medium" end
			end

			local defaultPostprocess = "Off";
			if(_POSTPROCESS)then defaultPostprocess = "On" end


			self.rowArray[1]:setDefault(defaultResolution)
			self.rowArray[2]:setDefault(defaultDisplay)
			self.rowArray[3]:setDefault(defaultMonitor)
			self.rowArray[4]:setDefault(defaultVsync)
			self.rowArray[5]:setDefault(defaultFSAA)
			self.rowArray[6]:setDefault(defaultFPS)
			self.rowArray[7]:setDefault(defaultQuality)
			self.rowArray[8]:setDefault(defaultPostprocess)
			----------------------End fet the defaults

		end
	end

	local applyChanges = function()
		--Menu:SetState("Options");
		local console = require("Engine/Console");
		local save = {};

		--monitor
		local monitor = list.rowArray[3].optionText:GetText();
		local command = "";
		if(monitor == "1")then 
			command = "monitor 1"
		elseif(monitor == "2")then 
			command = "monitor 2"
		elseif(monitor == "2")then 
			command = "monitor 2"
		end
		console:ForceProcess(command,true);
		save["Monitor"] = command;


		--The resolution changing
		local resolution = list.rowArray[1].optionText:GetText();
		
		local XY = {}
		if(resolution ~= "Native")then 
			XY = split(resolution,"x");
			XY[1] = tonumber(XY[1]); XY[2] = tonumber(XY[2]);
			Menu:PushMessage("ChangeResolution",XY);
		else 
			XY = {0,0}
			Menu:PushMessage("ChangeResolution",{0,0});
		end
		save["Resolution"] = "resolution " .. XY[1] .. "x" .. XY[2];

		--wmode
		local wmode = list.rowArray[2].optionText:GetText();
		local command = "";
		if(wmode == "Fullscreen")then 
			command = "wmode 1"
		elseif(wmode == "Windowed")then 
			command = "wmode 2"
		elseif(wmode == "Borderless")then 
			command = "wmode 3"
		end
		console:ForceProcess(command,true);
		save["Wmode"] = command;

		
		--vsync
		local vsync = list.rowArray[4].optionText:GetText();
		command = "";
		if(vsync == "Off")then 
			command = "vsync false"
		elseif(vsync == "On")then 
			command = "vsync true"
		end
		console:ForceProcess(command,true);
		save["Vsync"] = command;

		--antialiasing
		local FSAA = list.rowArray[5].optionText:GetText();
		command = "";
		if(FSAA == "Off")then 
			command = "antialiasing 0"
		elseif(FSAA == "FSAAx2")then 
			command = "antialiasing 2"
		elseif(FSAA == "FSAAx4")then 
			command = "antialiasing 4"
		elseif(FSAA == "FSAAx8")then 
			command = "antialiasing 8"
		elseif(FSAA == "FSAAx16")then 
			command = "antialiasing 16"
		end
		console:ForceProcess(command,true);
		save["FSAA"] = command;

		--fpscap
		local cap = list.rowArray[6].optionText:GetText();
		command = "";
		if(cap == "Unlimited")then 
			command = "set fpscap 0"
		else
			command = "set fpscap " .. cap; 
		end
		console:ForceProcess(command,true);
		save["Cap"] = command;

		--texture quality
		local quality = list.rowArray[7].optionText:GetText();
		command = "";
		if(quality == "Low")then 
			--command = "mipmap -1"
			command = "quality low"
		elseif(quality == "Medium")then 
			--command = "mipmap -.1"
			command = "quality medium"
		elseif(quality == "High")then 
			--command = "mipmap .5"
			command = "quality high"
		end
		console:ForceProcess(command,true)
		save["Quality"] = command;

		--postprocessing
		local postprocessing = list.rowArray[8].optionText:GetText();
		command = "";
		if(postprocessing == "Off")then 
			command = "set postprocess false"
		elseif(postprocessing == "On")then 
			command = "set postprocess true"
		end
		console:ForceProcess(command,true);
		save["PostProcess"] = command;


		--out of focus
		local focus = list.rowArray[9].optionText:GetText();
		command = "";
		if(focus == "Pause game")then 
		elseif(focus == "Keep game running")then
		elseif(focus == "Run at low fps")then 
		end
		console:ForceProcess(command,true)
		save["Focus"] = command;


		SaveStartup(save);
		list.done = nil;
		Menu:SetState("Options");
	end
	
	Menu:MakeButton("Back",X,925,function() list.done = nil; Menu:SetState("Options"); end,"back")
	Menu:MakeButton("Apply changes",1500,925,applyChanges,"confirm");

	h["VideoTitle"] = Menu:MakeText(X,100,"Video Options",65)
	StateData[State] = {contentHeight=630,content=true}

	--=======Audio Options=======--
	State = "Audio Options";


	local list = Menu:MakeList(EditorQuads);
	h["AudioList"] = list;
	StateLists[State] = list;
	list:AddMyItem("Master volume",nil,"Controls the volume for the entire game.",.5,{0,1});
	list:AddMyItem("Voice acting volume",nil,"Controls the volume of anything voice related.",.8,{0,1});
	list:AddMyItem("Sound effects volume",nil,"Controls the volume of the sound effects such as footsteps and button sounds.",.5,{0,1});
	list:AddMyItem("Music volume",nil,"Controls the volume of the background music.",.6,{0,1});
	list:AddMyItem("Stereo pan",{"Regular","Flipped"},"In case you are wearing your headphones backwards and you are too lazy to fix that.");
	for i=1,#list.rowArray do 
		list:Disable(list.rowArray[i])--disable them all cuz they dont work yet
	end

	local applyChanges = function()
		Menu:SetState("Options");	
	end

	Menu:MakeButton("Back",X,925,function() Menu:SetState("Options"); end,"back")
	Menu:MakeButton("Apply changes",1500,925,applyChanges,"confirm");
	h["AudioTitle"] = Menu:MakeText(X,100,"Audio Options",65)
	StateData[State] = {contentHeight=630,content=true}

	--=======Controls Options=======--
	State = "Controls Options";


	local list = Menu:MakeList(EditorQuads);
	h["ControlsList"] = list;
	StateLists[State] = list;

	

	local func = function() print("Do Something!") end

	list:AddBreak("Keyboards & mouse")
	list:AddMyItem("Key bindings",nil,"Click to edit key bindings.",nil,nil,{func,"Edit Keys"}); list:Disable(list.rowArray[#list.rowArray])
	list:AddMyItem("Mouse lock",{"Off","On"},"Mouse lock traps your cursor inside the game's window (allows for edge scrolling).",defaultMouselock);
	list:AddBreak("Controller")
	list:AddMyItem("Vibrations",{"Off","On"},"Enable/disable controller vibration."); list:Disable(list.rowArray[#list.rowArray])
	list:AddMyItem("Invert X Axis",{"Off","On"},"Seriously who does this?",2); list:Disable(list.rowArray[#list.rowArray])
	list:AddMyItem("Invert Y Axis",{"Off","On"},"Has anyone ever used this option? EVER?",2); list:Disable(list.rowArray[#list.rowArray])

	list.SetDefaults = function(self)
		if(self.done ~= true)then 
			self.done = true;
			local defaultMouselock = "Off";
			if(_Screen.mouseLock)then defaultMouselock = "On" end
			self.rowArray[2]:setDefault(defaultMouselock )
		end
	end

	local applyChanges = function()
		local console = require("Engine/Console");
		local save = {}

		--mouselock
		local mouselock = list.rowArray[2].optionText:GetText();
		local command = "";
		if(mouselock == "Off")then 
			command = "mouselock false"
			_Screen.mouseLock = false;
		elseif(mouselock == "On")then 
			command = "mouselock true"
			_Screen.mouseLock = true;
		end
		save["Mouselock"] = command;
		--console:ForceProcess(command,true);

		SaveStartup(save);
		Menu:SetState("Options");
		list.done = nil;
	end

	Menu:MakeButton("Back",X,925,function() list.done = nil; Menu:SetState("Options"); end,"back")
	Menu:MakeButton("Apply changes",1500,925,applyChanges,"confirm");
	h["ControlsTitle"] = Menu:MakeText(X,100,"Controls Options",65)
	StateData[State] = {contentHeight=630,content=true}

	--=======Party=======--
	State = "Party";

	local list = Menu:MakeList(EditorQuads);
	h["PartyList"] = list;
	StateLists[State] = list;
	local scores = {};
	for i=5,100,5 do 
		scores[#scores+1] = tostring(i);
	end
	list:AddMyItem("Max Score",scores,"First player to reach this score wins!","25");
	list:AddMyItem("Player Control",{"All Keyboard","All Controllers","Half Combined"},"Players input source: \"Half Combined\" means P1&2 on Keyboard and P3&4 on Controllers");

	--add list of game modes
	local gamelist = UIObject:newObject(160,700,"EditorUISheet","columnlist")
	gamelist.Owidth = 250; list.Oheight = 850
	gamelist:Rescale()
	gamelist:SetHeight(400)
	gamelist:SetMultiselectEnabled(true)
	gamelist:DisableButtons()
	gamelist.barX = -30 - gamelist.Owidth;
	gamelist.barY = 60;
	gamelist.disableInnerLines = true;

	gamelist:AddColumn("Game Modes",nil,1,nil,20);

	local partymode = require("Src/Modes/PartyMode");
	local gameModes = partymode:GetGameModes()
	local modeNames = {};
	for i,v in pairs(gameModes)do 
		if(i ~= "EndGame")then 
			modeNames[#modeNames+1] = {name=i,desc=v.desc}
		end
	end

	for i=1,#modeNames do 
		gamelist:AddRow(modeNames[i].name)
		gamelist.rowArray[#gamelist.rowArray].selected = true;
		gamelist.rowArray[#gamelist.rowArray].justSelected = true;
		local X = 430;
		local Y = 770;
		local desc = Menu:MakeText(X,Y + (i-1) * 40,modeNames[i].desc,20);
		desc:SetParent(list.parent);
		desc:SetColor({150,150,150,255})
		h["GameList" .. modeNames[i].desc] = desc;
	end

	gamelist:SetState(State)
	
	gamelist:SetRowHeight(40)
	gamelist:SetColumnHeight(60)
	h["GameList"] = gamelist;
	gamelist:SetParent(list.parent)

	gamelist.OnRowSelected = function(parent,row,data)
		local n = 0;

		for i=1,#parent.rowArray do 
			if(parent.rowArray[i].selected)then n = n + 1 end
		end

		if(row.justSelected and n > 1)then 
			row.justSelected = false;
			gamelist:DeselectRow(row)
		else
			row.justSelected = true;
		end
	end
	----End list of game mods

	Menu:MakeButton("Back",X,925,function() Menu:SetState(StateList[#StateList-1]); finishFunc = nil end,"back")
	
	h["PartyTitle"] = Menu:MakeText(X,100,"Party Options",65)
	StateData[State] = {contentHeight=630,content=true}

	local startParty = function()
		local maxScore = tonumber(list.rowArray[1].optionText:GetText());
		local controlType = list.rowArray[2].optionText:GetText()
		local exceptions = {};

		for i=1,#gamelist.rowArray do 
			local row = gamelist.rowArray[i];
			if(row.selected ~= true)then 
				exceptions[row.columndata[1]] = true;
			end
		end

		local data={numPlayers=2,maxScore=maxScore,exceptions=exceptions,controlType=controlType};
		finishFunc = function() Menu:startparty(data) end; 
		Menu:LoadAll()
		Menu:SetState("Loading")
	end
	Menu:MakeButton("Start Party!",1500,925,startParty,"confirm");
	--=======Loading=======--
	State = "Loading";
	
	Menu:MakeButton("Back",X,925,function() Menu:SetState(StateList[#StateList-1]); finishFunc = nil end,"back")
	h["Bar"] = Menu:MakeObject(150,850,"LoadingBar")
	h["LoadingTitle"] = Menu:MakeText(X,100,"Loading, please wait..",65)

	--=======Quit=======--
	State = "Quit";
	
	Menu:MakeButton("Back",X,925,function() Menu:SetState(StateList[#StateList-1]); end,"back")
	Menu:MakeButton("Quit game",1550,925,function() love.event.quit() end,"confirm")
	h["LoadingTitle"] = Menu:MakeText(X,100,"Exit to desktop",65)

	--=======LoadLevel=======--
	State = "LoadLevel";
	StateData[State] = {contentHeight=630,content=true}
	Menu:MakeButton("Back",X,925,function() Menu:SetState("Main") end,"back")
	button = Menu:MakeButton("Open levels folder",740,925,function() OpenSaveFolder("Levels/MyLevels") end,"alt")

	h["LoadLevelTitle"] = Menu:MakeText(X,100,"Load Level",65)

	local list = UIObject:newObject(160,250,"EditorUISheet","columnlist")
	list.Owidth = 1700; list.Oheight = 550
	list:Rescale()
	list:SetState(State)
	--newobject:AddColumn(name,NoTitle,customWidth,center,offset)
	list:AddColumn("Name",nil,.3,nil,20)
	list:AddColumn("Description",nil,.4,nil,20)
	list:AddColumn("Mode",nil,.1,true)
	list:AddColumn("Type",nil,.1,true)
	list:AddColumn("Ver",nil,.1,true)
	list:DisableButtons()
	list.barX = -40 - list.Owidth;
	list.barY = 70;
	list.customBarHeight = 480;
	h["LevelList"] = list;
	StateLists[State] = list;

	--get the actual levels
	
	local tileEngine = require("Engine/TileEngine")

	local populateLevelList = function()
		local files = love.filesystem.getDirectoryItems("Levels/MyLevels")
		for i=1,#files do 
			local filename = files[i];
			local index = string.find(filename,".level",1,true);
			if(index ~= nil)then 
				local levelname = filename:sub(1,index-1);
				local l = tileEngine:DecryptData("Levels/MyLevels/" .. filename);
				h["LevelList"]:AddRow(levelname,l.description,l.data.Mode,l.data.Type,tostring(l.gameVersion))
			end
		end
	end

	populateLevelList()


	list.Update = function(self)
		if(_Screen.refocus)then 
			self:Clear()
			populateLevelList()
			list:SetRowHeight(50);
			list:SetColumnHeight(70);
		end
	end

	list:SetRowHeight(50);
	list:SetColumnHeight(70);

	local loadLevel = function()
		if(currentLevel ~= "")then 
			finishFunc = function() Menu:startgame("Play",{"single",currentLevel}) end
			Menu:LoadAll()
			Menu:SetState("Loading")
		end
	end

	list.OnRowSelected = function(parent,row,data)
		currentLevel = data[1];
		local currentTime = love.timer.getTime();
		if(currentTime - doubleClickTime < .4)then
			loadLevel()
		end
		doubleClickTime = currentTime;
		for i=1,#list.rowArray do if(list.rowArray[i] == row)then selectedNum = i; break end end
	end

	Menu:MakeButton("Load level",1600,925,loadLevel,"confirm")

	--=======LoadList=======--
	State = "LoadList";
	StateData[State] = {contentHeight=630,content=true}
	Menu:MakeButton("Back",X,925,function() Menu:SetState("Main") end,"back")
	button = Menu:MakeButton("Open playlists folder",740,925,function() OpenSaveFolder("Levels/Playlists") end,"alt")

	h["LoadListTitle"] = Menu:MakeText(X,100,"Load Playlist",65)

	local list = UIObject:newObject(160,250,"EditorUISheet","columnlist")
	list.Owidth = 1700; list.Oheight = 550
	list:Rescale()
	list:SetState(State)
	--newobject:AddColumn(name,NoTitle,customWidth,center,offset)
	list:AddColumn("Name",nil,nil,nil,20)
	list:DisableButtons()
	list.barX = -40 - list.Owidth;
	list.barY = 70;
	list.customBarHeight = 480;
	h["PlayList"] = list;
	StateLists[State] = list;

	--get the actual levels
	
	local tileEngine = require("Engine/TileEngine")

	local populateList = function()
		local files = love.filesystem.getDirectoryItems("Levels/Playlists")
		for i=1,#files do 
			local filename = files[i];
			local index = string.find(filename,".txt",1,true);
			if(index ~= nil)then 
				local playlistname = filename:sub(1,index-1);
				list:AddRow(playlistname)
			end
		end
	end

	populateList()

	list.Update = function(self)
		if(_Screen.refocus)then 
			self:Clear()
			populateList()
			list:SetRowHeight(50);
			list:SetColumnHeight(70);
		end
	end

	list:SetRowHeight(50);
	list:SetColumnHeight(70);

	local loadLevel = function()
		if(currentList ~= "")then 
			finishFunc = function() Menu:startgame("Play",{"playlist",currentList}) end
			Menu:LoadAll()
			Menu:SetState("Loading")
		end
	end

	list.OnRowSelected = function(parent,row,data)
		currentList = data[1];
		local currentTime = love.timer.getTime();
		if(currentTime - doubleClickTime < .4)then
			loadLevel()
		end
		doubleClickTime = currentTime;
		for i=1,#list.rowArray do if(list.rowArray[i] == row)then selectedNum = i; break end end
	end

	Menu:MakeButton("Load level",1600,925,loadLevel,"confirm")

	-------------

	Menu:UpdateAllOnce();
	Menu:SetState("Main")
	
	
end

--[[
=============================================
=============================================
=========END The Menu States/Pages===========
=============================================
=============================================
]]--

local repeaters = {};
local delayCount = .4;
local delayRecover = .35;
local axisDone = false;
local axisHorizontalDone = false;
local MousePosition = {x=0,y=0}--keeps track of whether the mouse has moved
local mouseVisible = true;
local hatDown = {};

function Menu:AltInputUpdate(dt)--for alternate input
	if(repeaters.done ~= true)then 
		repeaters.done = true;
		repeaters["up"] = 0;
		repeaters["down"] = 0;
		repeaters["right"] = 0;
		repeaters["left"] = 0;
	end

	local xdis = _Mouse.x - MousePosition.x;
	local ydis = _Mouse.y - MousePosition.y;
	local dis = math.sqrt(xdis * xdis + ydis * ydis)
	if(dis > 5)then activeInput = "Mouse" end
	MousePosition.x = _Mouse.x;
	MousePosition.y = _Mouse.y;

	AltInput = {};

	local up = _KeyTap["w"] or _KeyTap["up"]
	local upHold = _KeyPress["w"] or _KeyPress["up"]
	local down = _KeyTap["s"] or _KeyTap["down"]
	local downHold = _KeyPress["s"] or _KeyPress["down"]
	local right = _KeyTap["d"] or _KeyTap["right"]
	local rightHold = _KeyPress["d"] or _KeyPress["right"]
	local left = _KeyTap["a"] or _KeyTap["left"]
	local leftHold = _KeyPress["a"] or _KeyPress["left"]

	local confirm = _KeyTap["return"];
	local back = _KeyTap["escape"] or _KeyTap["backspace"] or _KeyTap["delete"]
	local alt = _KeyTap["tab"];

	---check if any keyboard is pressed
	for i,v in pairs(_KeyTap)do 
		activeInput = "Keyboard"
	end

	---get controller 1
	local controller;
	for i,v in pairs(_JoySticks)do 
		if(v == 1)then controller = i;break end 
	end

	if(controller ~= nil)then 
		--- Xbox input ----
		if(controller:getGUID() == "00000000000000000000000000000000")then 
			--check if any xbox is pressed
			for i,v in pairs(_JoyTap[1])do 
				activeInput = "Xbox"
			end

			if(_JoyTap[1][1])then up = true end
			if(_JoyTap[1][2])then down = true end
			if(_JoyTap[1][3])then left = true end
			if(_JoyTap[1][4])then right = true end
			if(_JoyPress[1][1])then upHold = true end
			if(_JoyPress[1][2])then downHold = true end
			if(_JoyPress[1][3])then leftHold = true end
			if(_JoyPress[1][4])then rightHold = true end

			if(_JoyTap[1][11] or _JoyTap[1][5])then confirm = true end
			if(_JoyTap[1][12] or _JoyTap[1][6])then back = true end
			if(_JoyTap[1][14])then alt = true end
			local axis = controller:getAxis(2)
			local axisHorizontal = controller:getAxis(1)
			if(math.abs(axis) < .5)then axisDone = false end
			if(math.abs(axisHorizontal) < .5)then axisHorizontalDone = false end

			if(axis > 0.5)then 
				if(not axisDone)then down = true; axisDone = true end
				downHold = true 
				activeInput = "Xbox"
			end
			if(axis < -0.5)then 
				if(not axisDone)then up = true; axisDone = true end
				upHold = true 
				activeInput = "Xbox"
			end

			if(axisHorizontal > .5)then 
				if(not axisHorizontalDone)then right = true; axisHorizontalDone = true end
				rightHold = true;
				activeInput = "Xbox"
			end
			if(axisHorizontal < -.5)then 
				if(not axisHorizontalDone)then left = true; axisHorizontalDone = true end
				leftHold = true;
				activeInput = "Xbox"
			end

		end

		----- PS4 input -----
		if(controller:getGUID() == "4c05c405000000000000504944564944")then
			local hat = controller:getHat(1);
			if(hat ~= "c")then activeInput = "PS3" end

			--check if any ps3 is pressed
			for i,v in pairs(_JoyTap[1])do 
				activeInput = "PS3"
			end

			if(hat == "u" and not hatDown["u"])then up = true; hatDown["u"] = true end
			if(hat == "d" and not hatDown["d"])then down = true; hatDown["d"] = true; end
			if(hat == "r" and not hatDown["r"])then right = true; hatDown["r"] = true end
			if(hat == "l" and not hatDown["l"])then left = true; hatDown["l"] = true end

			if(hat == "u")then upHold = true else hatDown["u"] = false end
			if(hat == "d")then downHold = true else hatDown["d"] = false end
			if(hat == "r")then rightHold = true else hatDown["r"] = false end
			if(hat == "l")then leftHold = true else hatDown["l"] = false end

			if(_JoyTap[1][2])then confirm = true end
			if(_JoyTap[1][3])then back = true end
			if(_JoyTap[1][4])then alt = true end
			local axis = controller:getAxis(2)
			local axisHorizontal = controller:getAxis(1)
			if(math.abs(axis) < .5)then axisDone = false end
			if(math.abs(axisHorizontal) < .5)then axisHorizontalDone = false end
			if(axis > 0.5)then 
				if(not axisDone)then down = true; axisDone = true end
				downHold = true 
				activeInput = "PS3"
			end
			if(axis < -0.5)then 
				if(not axisDone)then up = true; axisDone = true end
				upHold = true 
				activeInput = "PS3"
			end
			if(axisHorizontal > .5)then 
				if(not axisHorizontalDone)then right = true; axisHorizontalDone = true end
				rightHold = true;
				activeInput = "PS3"
			end
			if(axisHorizontal < -.5)then 
				if(not axisHorizontalDone)then left = true; axisHorizontalDone = true end
				leftHold = true;
				activeInput = "PS3"
			end

		end

	end


	if(up)then AltInput["up"] = true end
	if(upHold and repeaters["up"] > delayCount)then AltInput["up"] = true; repeaters["up"] = delayRecover  end
	if(upHold)then repeaters["up"] = repeaters["up"] + dt else repeaters["up"] = 0 end

	if(down)then AltInput["down"] = true end
	if(downHold and repeaters["down"] > delayCount)then AltInput["down"] = true; repeaters["down"] = delayRecover end
	if(downHold)then repeaters["down"] = repeaters["down"] + dt else repeaters["down"] = 0 end

	if(right)then AltInput["right"] = true end
	if(rightHold and repeaters["right"] > delayCount)then AltInput["right"] = true; repeaters["right"] = delayRecover end
	if(rightHold)then repeaters["right"] = repeaters["right"] + dt else repeaters["right"] = 0 end

	if(left)then AltInput["left"] = true end
	if(leftHold and repeaters["left"] > delayCount)then AltInput["left"] = true; repeaters["left"] = delayRecover end
	if(leftHold)then repeaters["left"] = repeaters["left"] + dt else repeaters["left"] = 0 end

	if(openWindow.isDestroyed)then 
		if(confirm)then AltInput["confirm"] = true end
		if(back)then AltInput["back"] = true end
		if(alt)then AltInput["alt"] = true end
	end

	--Hide mouse if active input is not Mouse
	if(activeInput == "Mouse")then 
		if(mouseVisible == false)then 
			love.mouse.setVisible(true); mouseVisible = true 
		end
	else 
		if(mouseVisible == true)then 
			love.mouse.setVisible(false); mouseVisible = false; 
		end
	end

end



function Menu:HighlightsUpdate()
	local currentState = loveframes.GetState()
	local currentButtonList = StateButtons[currentState];

	---The confirm and back buttons
	if(AltInput["confirm"] and StateConfirm[currentState])then 
		StateConfirm[currentState]:onUp(); 
	end
	if(AltInput["back"] and StateBack[currentState])then 
		StateBack[currentState]:onUp(); 
	end
	if(AltInput["alt"] and StateAlt[currentState])then 
		StateAlt[currentState]:onUp();
	end
	----End confirm and back buttons

	if(currentButtonList ~= nil)then 
		for i=1,#currentButtonList do 
			currentButtonList[i].selected = false 
			if(currentButtonList[i].mouseHover)then selectedNum = 0 end
		end
		local selected = currentButtonList[selectedNum];
		if(selected ~= nil)then 
			selected.selected = true 
			if(AltInput["confirm"])then selected.onUp(); end
		end

		if(AltInput["down"])then 
			selectedNum = selectedNum + 1 
			if(selectedNum > #currentButtonList)then selectedNum = #currentButtonList end
		end
		if(AltInput["up"])then 
			selectedNum = selectedNum - 1 
			if(selectedNum < 1)then selectedNum = 1 end
		end
	end

	--The list controls
	local currentList = StateLists[currentState];
	if(currentList ~= nil)then 

		if(AltInput["down"])then 
			local selected = currentList.rowArray[selectedNum];
			local index = selectedNum;
			if(selected ~= nil)then index = selected.index end
			selectedNum = index + 1 
			doubleClickTime = 0;
			if(selectedNum > #currentList.rowArray)then selectedNum = #currentList.rowArray end
			selected = currentList.rowArray[selectedNum];
			currentList:SelectRow(selected)
		end
		if(AltInput["up"])then 
			local selected = currentList.rowArray[selectedNum];
			local index = selectedNum;
			if(selected ~= nil)then index = selected.index end
			selectedNum = index - 1 
			doubleClickTime = 0;
			if(selectedNum < 1)then selectedNum = 1 end
			selected = currentList.rowArray[selectedNum];
			currentList:SelectRow(selected)
		end

	end
end

function Menu:UpdateHighlightOnce()
	local currentState = loveframes.GetState()
	local currentList = StateLists[currentState];
	if(currentList ~= nil)then
		local selected = currentList.rowArray[selectedNum];
		currentList:SelectRow(selected)
	end
end

function Menu:UpdateAllOnce()
	for stateName,state in pairs(ObjStates)do 
		for i=1,#state do 
			state[i]:update();
		end 
	end
end

function Menu:SetState(name)
	for stateName,state in pairs(ObjStates)do 
		for i=1,#state do 
			if(stateName ~= name)then 
				--disable
				if(state[i].fauxrender == nil)then state[i].fauxrender = state[i].render; end
				if(state[i].fauxupdate == nil)then state[i].fauxupdate = state[i].update; end
				state[i].render = function() end
				state[i].update = function() end
			else 
				--enable
				if(state[i].fauxupdate)then state[i].update = state[i].fauxupdate; end
				if(state[i].fauxrender)then state[i].render = state[i].fauxrender; end
			end 
		end
	end

	loveframes.SetState(name);

	StateList[#StateList+1] = name;

	if(activeInput ~= "Mouse")then 
		selectedNum = 1;
		Menu:UpdateHighlightOnce()
	end
end

local MakeStrip = function(x,y,height)
	local Strip = {x=x,y=y,height=height,depth=1,layer=2,visible = true};
	Strip.render = function(s)
		if(_SHADER)then 
			love.graphics.setShader() --disable shader just for me plz
		end

		local f = (_Screen.width/1920)
		if(not s.visible)then return end
		love.graphics.setColor(0,0,0,150)
		love.graphics.rectangle("fill",s.x*f,s.y*f,(1920 - s.x * 2)*f,s.height*f)
		love.graphics.setColor(255,255,255,255)

		if(_SHADER)then 
			love.graphics.setShader(_ShaderArray["ColorTransform"]) --putting it back
		end
	end
	Strip.update = function() end
	Strip.Destroy = function(s)
		for i=1,#_miscUIRenders do 
			if(_miscUIRenders[i] == s)then table.remove(_miscUIRenders,i); break end
		end
	end
	_miscUIRenders[#_miscUIRenders+1] = Strip;
	return Strip;
end

local fadeAlpha = 1;


function Menu:init(l)
	--initialize everything and the loader object
	loader = l;--This is to check that things are loaded, if not, load them
	--Since the menu is responsible resource management (It checks if a level requires certain assets to be loaded before loading it)

	--set the loveframes menu skin
	loveframes.util.SetActiveSkin("Menu");
	self:SetState("Main");

	dynamicCreate("Background2",960,540,"MenuBg1")
	dynamicCreate("Background",960,540,"MenuBg1")

	--Create those black strips
	h["TopStrip"] = MakeStrip(0,30,150);
	h["ContentStrip"] = MakeStrip(0,220,630);
	h["BottomStrip"] = MakeStrip(0,450,50);

	fadeAlpha = 1;
	bgCounter = 0;
	currentBg = 1;

	initMenu = false;
	finishFunc = nil;
	love.mouse.setCursor() 

	selectedNum = 0;

	
end

local doneTest = false;

function Menu:update(dt)

	if(not doneTest and _Images["Joe"] ~= nil)then 
		doneTest = true 
		testobj = gameobj:newUIObject(0,0,"Joe");
		local up = function(self)
			local f = 1920/_Screen.width;
			local fY = 1080/_Screen.height;
			local mx,my = _Mouse.x*f,_Mouse.y*f;

			--self.x = mx; 
			--self.y = my;
		end
		testobj.visible = false
		testobj.layer = 10;
		h["BlargMarg"] = testobj;
		
		testobj:AddUpdate(up);
	end

	createObjects();
	changingBackground(dt)
	self:AltInputUpdate(dt)
	self:HighlightsUpdate()

	if(not initMenu and _Images["MainMenuSheet"])then 
		makeMenuObjects()
		initMenu = true;
	end

	if(initMenu)then 
		h["Bar"]:SetScaleXY(1620 * loader:getProgress())
	end

	if(finishFunc ~= nil and loader:getProgress() == 1)then 
		finishFunc()
	end

	if(initMenu and fadeAlpha > 0)then 
		fadeAlpha = fadeAlpha - dt * 2
		if(fadeAlpha < 0)then fadeAlpha = 0 end
	end


	---Strips update
	local state = loveframes.GetState();
	local showtop = false;--the defaults
	local showbottom = false;
	local showContent = false;
	local contentHeight = 630;
	local topHeight = 150;
	if(StateData[state] ~= nil)then 
		local d = StateData[state];
		if(d.top ~= nil)then showtop = d.top end
		if(d.bottom ~= nil)then showbottom = d.bottom end
		if(d.topHeight ~= nil)then topHeight = d.topHeight end
		if(d.content ~= nil)then showContent = d.content end
		if(d.contentHeight ~= nil)then contentHeight = d.contentHeight end
	end

	h["TopStrip"].height = topHeight;
	h["TopStrip"].visible = showtop;
	h["BottomStrip"].visible = showbottom;
	h["ContentStrip"].visible = showContent;
	h["ContentStrip"].height = contentHeight;

	--make the system cursor go to a hand for buttons
	local shouldHover = false;
	for i=1,#buttons do 
		local b = buttons[i];
		if(b:GetState() == state and b.mouseHover and b.enabled ~= false)then shouldHover = true; break end
	end
	if(shouldHover)then love.mouse.setCursor(cursor) else love.mouse.setCursor() end
end

function Menu:render()
	---in case you need to manually render something
	love.graphics.setColor(0,0,0,255 * fadeAlpha)
	love.graphics.rectangle("fill",0,0,_Screen.width,_Screen.height)
	love.graphics.setColor(255,255,255,255)
end

function Menu:destroy()
	--clears everything about the menu
	--to be run when switching modes
	loveframes.util.SetActiveSkin("Main");
	loveframes.SetState("none");
	SetCursor()

	for i,v in pairs(h)do 
		v:Destroy() 
	end
	h = {};
	for i=1,#buttons do buttons[i]:Destroy() end
	buttons = {};
	for i=1,#prompts do prompts[i]:Destroy() end
	prompts = {};
	StateButtons = {};
	ObjStates = {};
	love.mouse.setVisible(true);

	if(openWindow.isDestroyed ~= true)then openWindow:Destroy();end
end

--------------------------------------
--====================================
--====== Menu callbacks here =========
--====================================
--------------------------------------
--This are made to be overwritten by the parent class 

function Menu:startgame(mode,modeArgs)
	--mode is either single or playlist
	--modeArgs is a table, modeArgs[1] is either single or playlist, modeArgs[2] is the name of level or playlist respectively
end

function Menu:starteditor()
end

function Menu:startparty(modeArgs)
end

function Menu:LoadAll()
end

return Menu;